package customer;

import java.util.Scanner;

public class Customer {
    // five attributes
    private static int custCounter;
    private int custId;
    private String custName;
    private String custType;
    private int custAge;
    private long phoneNumber;

    

        // Method 1 to set the data for all attributes
    public void setData(){
        Scanner in = new Scanner(System.in);
        
        //Auto Incrementing each custId when adding new customer
        custId=++custCounter;
        
        System.out.print("Enter Customer name: ");
        custName = in.nextLine();

        System.out.print("Enter customer type: ");
        custType = in.next();
        
        System.out.print("Enter customer age: ");
        custAge = in.nextInt();

        System.out.print("Enter phone number: ");
        phoneNumber = in.nextLong();

        in.close();
    }

    // Method 2 to display the data of all attributes
    public void getData(){
        System.out.println("id: "+custId);
        System.out.println("Name: "+custName);
        System.out.println("Type of Customer: "+custType);
        System.out.println("Age: "+custAge);
        System.out.println("Phone Number: "+phoneNumber);

    }

}
