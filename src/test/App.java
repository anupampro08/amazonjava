package test;

import customer.Customer;
import product.Product;

public class App {
    public static void main(String[] args) throws Exception {
            Customer c1 = new Customer();
            Customer c2 = new Customer();

            System.out.println("Enter data for Customer 1");
            c1.setData();

            System.out.println("\nEnter data for Customer 2");
            c2.setData();

            System.out.println("\nCustomer 1 Data");
            c1.getData();

            System.out.println("\nCustomer 2 Data");
            c2.getData();

            Product p1 = new Product();
            Product p2 = new Product();


            System.out.println("\nEnter data for Product 1");
            p1.setData();
            
            System.out.println("\nEnter data for Product 2");
            p2.setData();

            System.out.println("\nProduct 1 Data");
            p1.getData();

            System.out.println("\nProduct 2 Data");
            p2.getData();
        }


}
