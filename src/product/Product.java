package product;

import java.util.Scanner;

public class Product {
    private static int prodCounter;
    private int prodId;
    private String prodName;
    private double prodPrice;
    private String category;
    private String brandName;



    // Method 1 to set the data for all attributes
    public void setData(){
        Scanner in = new Scanner(System.in);
        
        //Auto Incrementing each custId when adding new customer
        prodId=++prodCounter;
        
        System.out.print("Enter Product name: ");
        prodName = in.nextLine();

        System.out.print("Enter product price: ");
        prodPrice = in.nextDouble();
        
        System.out.print("Enter category: ");
        category = in.next();

        System.out.print("Enter brand name: ");
        brandName = in.next();

        in.close();
    }

    // Method 2 to display the data of all attributes
    public void getData(){
        System.out.println("Product id: "+prodId);
        System.out.println("Product Name: "+prodName);
        System.out.println("Type of Product: "+prodPrice);
        System.out.println("Price of Product: "+category);
        System.out.println("Brand Name : "+brandName);

    }

}
